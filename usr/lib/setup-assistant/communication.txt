true
telegram-desktop
Telegram Desktop
false
discord
Discord
false
element-desktop
Element (Matrix client)
false
wire-desktop
Wire (E2EE messenger)
false
signal-desktop
Signal Desktop
false
jitsi-meet
Jitsi-Meet (Open source webconferencing)
false
zoom
Zoom (Proprietary webconferencing)
false
skypeforlinux-stable-bin
Skype
false
slack-desktop
Slack (Messenger)
false
mumble
Mumble (Open source Teamspeak clone)
